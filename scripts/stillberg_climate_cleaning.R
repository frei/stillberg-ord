# INFO ----

# This script is used to compare the Stillberg climate datasets to figure out
# a) where values of the same parameter and same timestamp differ between stations
# b) where there are time gaps in one station that can be filled with info from another station
# Author: Lia Lechler
# Starting Date: 24/02/2023

# Fragestellungen:
# a) Gibt es Lücken in einem Datensatz im Vergleich zu dem anderen?
# b) Wenn beide kein No data haben, wie gross ist die Differenz.
# c) Dann checken ob verschiedene Genauigkeit oder andere Sensoren.


# SETTING UP WORKSPACE ----

# Clean workspace
rm(list = ls())

# Load packages useful for data manipulation
library(arsenal)
library(dplyr)
library(generics)
library(ggplot2)

# Set working directory
setwd("C:/Git/stillberg-ord")

# Read .txt files for different climate stations, add back in column names

stb_kli <- read.table("data/old/climate/STB_Kli.txt", header = FALSE, sep = "")
# timestamp DW HS ISWR ISWR_SLOPE PSUM RH_THY RSWR TA10 TA100 TA50 TA_ASP TA_HUTTE TA_THY TA_TREE TD_THY TS10 TS50 TSG VW VW_MAX
colnames(stb_kli) <- c("timestamp", "DW", "HS", "ISWR", "ISWR_SLOPE", "PSUM", "RH_THY", "RSWR", "TA10",
                       "TA100", "TA50", "TA_ASP", "TA_HUTTE", "TA_THY", "TA_TREE", "TD_THY",
                       "TS10", "TS50", "TSG", "VW", "VW_MAX")


stb_1 <- read.table("data/old/climate/STB1.txt", header = FALSE, sep = "")
# timestamp DW HS ISWR ISWR_SLOPE P PSUM RH_ROC RH_ROT RSWR TA10 TA100 TA50 TA_ASP TA_HUTTE TA_ROT TA_TREE TS10 TS50 TSG VW VW_MAX
colnames(stb_1) <- c("timestamp", "DW", "HS", "ISWR", "ISWR_SLOPE", "P", "PSUM", "RH_ROC", "RH_ROT", "RSWR", "TA10",
                     "TA100", "TA50", "TA_ASP", "TA_HUTTE", "TA_ROT", "TA_TREE",
                     "TS10", "TS50", "TSG", "VW", "VW_MAX")


stb_2_new <- read.table("data/old/climate/STB2_new.txt", header = FALSE, sep = "")
# timestamp DW HS HS_2 ISWR ISWR_SLOPE P PSUM RH_ROT RH_THY RSWR TA_10 TA_100 TA_50 TA_HUTTE TA_ROT TA_THY TA_TREE TS10 TS100 TS50 TSG VW VW_MAX
colnames(stb_2_new) <- c("timestamp", "DW", "HS", "HS_2", "ISWR", "ISWR_SLOPE", "P", "PSUM", "RH_ROT", "RH_THY", "RSWR", "TA10",
                         "TA100", "TA50", "TA_HUTTE", "TA_ROT", "TA_THY", "TA_TREE",
                         "TS10", "TS100", "TS50", "TSG", "VW", "VW_MAX")
# NOTE: renamed variables TA so they are in line with STB_1


# stb_2_old <- read.table("data/climate/STB2_old_bebi.txt", header = FALSE, sep = "")
# timestamp DW DW_SD HS ILWR ISWR P PSUM RH RH_2 RSWR TA TA_2 TSG TSOIL_10 TSOIL_20 TSOIL_50 VW VW_MAX
# colnames(stb_2_old) <- c("timestamp", "DW", "DW_SD", "HS", "ILWR", "ISWR", "P", "PSUM", "RH", "RH_2", "RSWR", "TA",
# "TA_2", "TSG", "TSOIL_10", "TSOIL_20", "TSOIL_50", "VW", "VW_MAX")


# FIND MATCHING/DIFFERING RECORDS ----

# For our paper, only look at: air temp (TA), precipitation (PSUM), humidity (RH), radiation (ISWR, ILWR, RSWR) and snow depth (HS)
# stb_kli = 01.01.1997 - 20.09.2000 ; why? Klimet was started in 1991 initially (probably because switch to 10min)
# stb_1 = 26.11.1996 - 30.09.2005
# stb_2_new = 28.09.2000 - 30.09.2005 ; until 2023 is already cleaned and in MB's records
# stb_2_old = 26.11.1996 - 30.09.2020

# Use comparedf function of arsenal package # BUT: too many differing values to be useful
# summary(comparedf(stb_2_new, stb_2_old))
# summary(comparedf(stb_2_new, stb_1))
# summary(comparedf(stb_1, stb_kli))

# Use all equal function from dplyr
# all.equal(stb_2_new, stb_1)

# Use join function to get overlapping timestamps
stb_a <- inner_join(stb_kli, stb_1, by = c("timestamp"))
stb_b <- inner_join(stb_1, stb_2_new, by = c("timestamp"))


# COMPARE STB KLI AND STB 1 ----

# Plot graphs to see how well the variables align
# stb_a_filter <- filter_all(stb_a, all_vars(. != "-999"))
# (plot_dw <- ggplot(data = stb_a_filter) +
# geom_point(aes(x = timestamp, y = DW.x), size = 0.01, colour = c("darkgrey")) +
# geom_point(aes(x = timestamp, y = DW.y), size = 0.01, colour = c("blue")))

# Match dataframes under a certain condition
# stb_a_psum <- stb_kli %>% 
# anti_join(stb_1, by = c("PSUM" = "PSUM"))

# Mutate temperature variables first into celsius so both datasets are at the same scales
stb_a_mut <- stb_a %>% 
  mutate(TA10.x = TA10.x * 0.1) %>% 
  mutate(TA100.x = TA100.x * 0.1) %>%
  mutate(TA50.x = TA50.x * 0.1) %>%
  mutate(TA_ASP.x = TA_ASP.x * 0.1) %>%
  mutate(TA_HUTTE.x = TA_HUTTE.x * 0.1) %>%
  mutate(TA_TREE.x = TA_TREE.x * 0.1) %>%
  mutate(TS10.x = TS10.x * 0.1) %>%
  mutate(TS50.x = TS50.x * 0.1) %>% 
  mutate(TSG.x = TSG.x * 0.1) 

# Mutate temperature variables into Kelvin so both datasets are at the same scales
stb_a_mut_2 <- stb_a_mut %>% 
  mutate(TA10.x = TA10.x + 273.15) %>% 
  mutate(TA100.x = TA100.x + 273.15) %>%
  mutate(TA50.x = TA50.x + 273.15) %>%
  mutate(TA_ASP.x = TA_ASP.x + 273.15) %>%
  mutate(TA_HUTTE.x = TA_HUTTE.x + 273.15) %>%
  mutate(TA_TREE.x = TA_TREE.x + 273.15) %>%
  mutate(TS10.x = TS10.x + 273.15) %>%
  mutate(TS50.x = TS50.x + 273.15) %>% 
  mutate(TSG.x = TSG.x + 273.15)

# Create new colum that shows whether there is a difference the columns
stb_a_sum <- stb_a_mut_2 %>% 
  mutate(record_DW = if_else(DW.x == DW.y, "same", "different")) %>%  
  mutate(record_HS = if_else(HS.x == HS.y, "same", "different")) %>% 
  mutate(record_ISWR = if_else(ISWR.x == ISWR.y, "same", "different")) %>% 
  mutate(record_ISWR_SLOPE = if_else(ISWR_SLOPE.x == ISWR_SLOPE.y, "same", "different")) %>%
  mutate(record_PSUM = if_else(PSUM.x == PSUM.y, "same", "different")) %>%
  mutate(record_RSWR = if_else(RSWR.x == RSWR.y, "same", "different")) %>%
  mutate(record_TA10 = if_else(TA10.x == TA10.y, "same", "different")) %>%
  mutate(record_TA100 = if_else(TA100.x == TA100.y, "same", "different")) %>%
  mutate(record_TA50 = if_else(TA50.x == TA50.y, "same", "different")) %>%
  mutate(record_TA_ASP = if_else(TA_ASP.x == TA_ASP.y, "same", "different")) %>%
  mutate(record_TA_HUTTE = if_else(TA_HUTTE.x == TA_HUTTE.y, "same", "different")) %>%
  mutate(record_TA_TREE = if_else(TA_TREE.x == TA_TREE.y, "same", "different")) %>%
  mutate(record_TS10 = if_else(TS10.x == TS10.y, "same", "different")) %>%
  mutate(record_TS50 = if_else(TS50.x == TS50.y, "same", "different")) %>%
  mutate(record_TSG = if_else(TSG.x == TSG.y, "same", "different")) %>%
  mutate(record_VW = if_else(VW.x == VW.y, "same", "different")) %>%
  mutate(record_VW_MAX = if_else(VW_MAX.x == VW_MAX.y, "same", "different"))

# Create new colum that shows how large the difference is
stb_a_sum_2 <- stb_a_sum %>% 
  mutate(difference_DW = DW.y - DW.x) %>% 
  mutate(difference_HS = HS.y - HS.x) %>%
  mutate(difference_ISWR = ISWR.y - ISWR.x) %>%
  mutate(difference_ISWR_SLOPE = ISWR_SLOPE.y - ISWR_SLOPE.x) %>%
  mutate(difference_PSUM = PSUM.y - PSUM.x) %>%
  mutate(difference_RSWR= RSWR.y - RSWR.x) %>%
  mutate(difference_TA10 = TA10.y - TA10.x) %>%
  mutate(difference_TA100 = TA100.y - TA100.x) %>%
  mutate(difference_TA50 = TA50.y - TA50.x) %>%
  mutate(difference_TA_ASP = TA_ASP.y - TA_ASP.x) %>%
  mutate(difference_TA_HUTTE = TA_HUTTE.y - TA_HUTTE.x) %>%
  mutate(difference_TA_TREE = TA_TREE.y - TA_TREE.x) %>%
  mutate(difference_TS10 = TS10.y - TS10.x) %>%
  mutate(difference_TS50 = TS50.y - TS50.x) %>%
  mutate(difference_TSG = TSG.y - TSG.x) %>%
  mutate(difference_VW = VW.y - VW.x) %>%
  mutate(difference_VW_MAX = VW_MAX.y - VW_MAX.x)


# EXPORT CSV FOR CHECKING ----
# write.csv(stb_a_sum_2, "outputs/climate/20230307_stb_kli_stb_1.csv")


# COMPARE PARAMETERS ----

# 1) Column DW (wind direction, unit = degrees)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_DW), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_dw_a <- subset(stb_a_sum_2, DW.x == -999 | DW.y == -999)

# Identify similar records
fil_dw_b <- subset(stb_a_sum_2, record_DW == "same")


# 2) Column HS (snow height, unit = meters)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_HS), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_hs_a <- subset(stb_a_sum_2, HS.x == -999 | HS.y == -999)

# Identify similar records
fil_hs_b <- subset(stb_a_sum_2, record_HS == "same")


# 3) Column ISWR (in short wave rad, unit = W/m2)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_ISWR), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_iswr_a <- subset(stb_a_sum_2, ISWR.x == -999 | ISWR.y == -999)

# Identify similar records
fil_iswr_b <- subset(stb_a_sum_2, record_ISWR == "same")


# 4) Column P_SUM (precipitation, unit = kg/m2)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_PSUM), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_psum_a <- subset(stb_a_sum_2, PSUM.x == -999 | PSUM.y == -999)

# Identify similar records
fil_psum_b <- subset(stb_a_sum_2, record_PSUM == "same")


# 5) Column RSWR (ref short wave rad, unit = W/m2)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_RSWR), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_rswr_a <- subset(stb_a_sum_2, RSWR.x == -999 | RSWR.y == -999)

# Identify similar records
fil_rswr_b <- subset(stb_a_sum_2, record_RSWR == "same")


# 6) Column TA10 (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA10), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta10_a <- subset(stb_a_sum_2, TA10.x == -999 | TA10.y == -999)

# Identify similar records
fil_ta10_b <- subset(stb_a_sum_2, record_TA10 == "same")


# 7) Column TA100 (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA100), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta100_a <- subset(stb_a_sum_2, TA100.x == -999 | TA100.y == -999)

# Identify similar records
fil_ta100_b <- subset(stb_a_sum_2, record_TA100 == "same")


# 8) Column TA50 (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA50), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta50_a <- subset(stb_a_sum_2, TA50.x == -999 | TA50.y == -999)

# Identify similar records
fil_ta50_b <- subset(stb_a_sum_2, record_TA50 == "same")


# 9) Column TA_ASP (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA_ASP), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta_asp_a<- subset(stb_a_sum_2, TA_ASP.x == -999 | TA_ASP.y == -999)

# Identify similar records
fil_ta_asp_b <- subset(stb_a_sum_2, record_TA_ASP == "same")


# 10) Column TA_HUTTE (air temperature hut, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA_HUTTE), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta_hutte_a <- subset(stb_a_sum_2, TA_HUTTE.x == -999 | TA_HUTTE.y == -999)

# Identify similar records
fil_ta_hutte_b <- subset(stb_a_sum_2, record_TA_HUTTE == "same")


# 11) Column TA_TREE (air temperature tree, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA_TREE), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta_tree_a <- subset(stb_a_sum_2, TA_TREE.x == -999 | TA_TREE.y == -999)

# Identify similar records
fil_ta_tree_b <- subset(stb_a_sum_2, record_TA_TREE == "same")


# 12) Column TS10 (soil temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TS10), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ts10_a <- subset(stb_a_sum_2, TS10.x == -999 | TS10.y == -999)

# Identify similar records
fil_ts10_b <- subset(stb_a_sum_2, record_TS10 == "same")


# 13) Column TS50 (soil temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TS50), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ts50_a <- subset(stb_a_sum_2, TS50.x == -999 | TS50.y == -999)

# Identify similar records
fil_ts50_b <- subset(stb_a_sum_2, record_TS50 == "same")


# 14) Column TSG (ground soil temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TSG), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_tsg_a <- subset(stb_a_sum_2, TSG.x == -999 | TSG.y == -999)

# Identify similar records
fil_tsg_b <- subset(stb_a_sum_2, record_TSG == "same")


# 15) Column VW (wind velocity, unit = m/s)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_VW), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_vw_a <- subset(stb_a_sum_2, VW.x == -999 | VW.y == -999)

# Identify similar records
fil_vw_b <- subset(stb_a_sum_2, record_VW == "same")


# 16) Column VW_MAX (maximum wind velocity, unit = m/s)

# Plot for difference distribution
ggplot(data = stb_a_sum_2) +
  geom_point(aes(x = timestamp, y = difference_VW_MAX), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_vw_max_a <- subset(stb_a_sum_2, VW_MAX.x == -999 | VW_MAX.y == -999)

# Identify similar records
fil_vw_max_b <- subset(stb_a_sum_2, record_VW_MAX == "same")




# COMPARE STB 1 AND STB 2 ----

# Create new colum that shows whether there is a difference the columns
stb_b_sum <- stb_b %>% 
  mutate(record_DW = if_else(DW.x == DW.y, "same", "different")) %>%  
  mutate(record_HS = if_else(HS.x == HS.y, "same", "different")) %>% 
  mutate(record_ISWR = if_else(ISWR.x == ISWR.y, "same", "different")) %>% 
  mutate(record_ISWR_SLOPE = if_else(ISWR_SLOPE.x == ISWR_SLOPE.y, "same", "different")) %>%
  mutate(record_P = if_else(P.x == P.y, "same", "different")) %>%
  mutate(record_PSUM = if_else(PSUM.x == PSUM.y, "same", "different")) %>%
  mutate(record_RH_ROT = if_else(RH_ROT.x == RH_ROT.y, "same", "different")) %>%
  mutate(record_RSWR = if_else(RSWR.x == RSWR.y, "same", "different")) %>%
  mutate(record_TA10 = if_else(TA10.x == TA10.y, "same", "different")) %>%
  mutate(record_TA100 = if_else(TA100.x == TA100.y, "same", "different")) %>%
  mutate(record_TA50 = if_else(TA50.x == TA50.y, "same", "different")) %>%
  mutate(record_TA_HUTTE = if_else(TA_HUTTE.x == TA_HUTTE.y, "same", "different")) %>%
  mutate(record_TA_ROT = if_else(TA_ROT.x == TA_ROT.y, "same", "different")) %>%
  mutate(record_TA_TREE = if_else(TA_TREE.x == TA_TREE.y, "same", "different")) %>%
  mutate(record_TS_10 = if_else(TS10.x == TS10.y, "same", "different")) %>%
  mutate(record_TS_50 = if_else(TS50.x == TS50.y, "same", "different")) %>%
  mutate(record_TSG = if_else(TSG.x == TSG.y, "same", "different")) %>%
  mutate(record_VW = if_else(VW.x == VW.y, "same", "different")) %>%
  mutate(record_VW_MAX = if_else(VW_MAX.x == VW_MAX.y, "same", "different"))

# Create new colum that shows how large the difference is
stb_b_sum_2 <- stb_b_sum %>% 
  mutate(difference_DW = DW.y - DW.x) %>% 
  mutate(difference_HS = HS.y - HS.x) %>%
  mutate(difference_ISWR = ISWR.y - ISWR.x) %>%
  mutate(difference_ISWR_SLOPE = ISWR_SLOPE.y - ISWR_SLOPE.x) %>%
  mutate(difference_P = P.y - P.x) %>%
  mutate(difference_PSUM = PSUM.y - PSUM.x) %>%
  mutate(difference_RH_ROT = RH_ROT.y - RH_ROT.x) %>%
  mutate(difference_RSWR= RSWR.y - RSWR.x) %>%
  mutate(difference_TA10 = TA10.y - TA10.x) %>%
  mutate(difference_TA100 = TA100.y - TA100.x) %>%
  mutate(difference_TA50 = TA50.y - TA50.x) %>%
  mutate(difference_TA_HUTTE = TA_HUTTE.y - TA_HUTTE.x) %>%
  mutate(difference_TA_ROT = TA_ROT.y - TA_ROT.x) %>%
  mutate(difference_TA_TREE = TA_TREE.y - TA_TREE.x) %>%
  mutate(difference_TS10 = TS10.y - TS10.x) %>%
  mutate(difference_TS50 = TS50.y - TS50.x) %>%
  mutate(difference_TSG = TSG.y - TSG.x) %>%
  mutate(difference_VW = VW.y - VW.x) %>%
  mutate(difference_VW_MAX = VW_MAX.y - VW_MAX.x)


# EXPORT CSV FOR CHECKING ----
# write.csv(stb_b_sum_2, "outputs/climate/20230309_stb_1_stb_2.csv")


# COMPARE PARAMETERS ----

# 1) Column DW (wind direction, unit = degrees)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_DW), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_dw_1 <- subset(stb_b_sum_2, DW.x == -999 | DW.y == -999)

# Identify similar records
fil_dw_2 <- subset(stb_b_sum_2, record_DW == "same")

# Identify records between 300 and 350
fil_dw_3 <- filter(stb_b_sum_2, between(difference_DW, 300, 350))


# 2) Column HS (snow height, unit = meters)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_HS), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_hs_1 <- subset(stb_b_sum_2, HS.x == -999 | HS.y == -999)

# Identify similar records
fil_hs_2 <- subset(stb_b_sum_2, record_HS == "same")


# 3) Column ISWR (in short wave rad, unit = W/m2)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_ISWR), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_iswr_1 <- subset(stb_b_sum_2, ISWR.x == -999 | ISWR.y == -999)

# Identify similar records
fil_iswr_2 <- subset(stb_b_sum_2, record_ISWR == "same")


# 4) Column P (air pressure, unit = pascal)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_P), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_p_1 <- subset(stb_b_sum_2, P.x == -999 | P.y == -999)

# Identify columns with +100/-100 difference
fil_p_2 <- subset(stb_b_sum_2, difference_P == -100 | difference_P == 100)

# Identify similar records
fil_p_3 <- subset(stb_b_sum_2, record_P == "same")


# 5) Column P_SUM (precipitation, unit = kg/m2)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_PSUM), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_psum_1 <- subset(stb_b_sum_2, PSUM.x == -999 | PSUM.y == -999)

# Identify similar records
fil_psum_2 <- subset(stb_b_sum_2, record_PSUM == "same")


# 6) Column RH_ROT (relative humidity, unit = ??)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_RH_ROT), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_rh_rot_1 <- subset(stb_b_sum_2, RH_ROT.x == -999 | RH_ROT.y == -999)

# Identify similar records
fil_rh_rot_2 <- subset(stb_b_sum_2, record_RH_ROT == "same")


# 7) Column RSWR (ref short wave rad, unit = W/m2)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_RSWR), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_rswr_1 <- subset(stb_b_sum_2, RSWR.x == -999 | RSWR.y == -999)

# Identify similar records
fil_rswr_2 <- subset(stb_b_sum_2, record_RSWR == "same")


# 8) Column TA10 (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA10), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta10_1 <- subset(stb_b_sum_2, TA10.x == -999 | TA10.y == -999)

# Identify similar records
fil_ta10_2 <- subset(stb_b_sum_2, record_TA10 == "same")


# 9) Column TA100 (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA100), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta100_1 <- subset(stb_b_sum_2, TA100.x == -999 | TA100.y == -999)

# Identify similar records
fil_ta100_2 <- subset(stb_b_sum_2, record_TA100 == "same")


# 10) Column TA50 (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA50), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta50_1 <- subset(stb_b_sum_2, TA50.x == -999 | TA50.y == -999)

# Identify similar records
fil_ta50_2 <- subset(stb_b_sum_2, record_TA50 == "same")


# 11) Column TA_HUTTE (air temperature hut, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA_HUTTE), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta_hutte_1 <- subset(stb_b_sum_2, TA_HUTTE.x == -999 | TA_HUTTE.y == -999)

# Identify similar records
fil_ta_hutte_2 <- subset(stb_b_sum_2, record_TA_HUTTE == "same")


# 12) Column TA_ROT (air temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA_ROT), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta_rot_1 <- subset(stb_b_sum_2, TA_ROT.x == -999 | TA_ROT.y == -999)

# Identify similar records
fil_ta_rot_2 <- subset(stb_b_sum_2, record_TA_ROT == "same")


# 13) Column TA_TREE (air temperature tree, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TA_TREE), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ta_tree_1 <- subset(stb_b_sum_2, TA_TREE.x == -999 | TA_TREE.y == -999)

# Identify similar records
fil_ta_tree_2 <- subset(stb_b_sum_2, record_TA_TREE == "same")


# 14) Column TS10 (soil temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TS10), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ts10_1 <- subset(stb_b_sum_2, TS10.x == -999 | TS10.y == -999)

# Identify similar records
fil_ts10_2 <- subset(stb_b_sum_2, record_TS10 == "same")


# 15) Column TS50 (soil temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TS50), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_ts50_1 <- subset(stb_b_sum_2, TS50.x == -999 | TS50.y == -999)

# Identify similar records
fil_ts50_2 <- subset(stb_b_sum_2, record_TS50 == "same")


# 16) Column TSG (ground soil temperature, unit = K)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_TSG), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_tsg_1 <- subset(stb_b_sum_2, TSG.x == -999 | TSG.y == -999)

# Identify similar records
fil_tsg_2 <- subset(stb_b_sum_2, record_TSG == "same")


# 17) Column VW (wind velocity, unit = m/s)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_VW), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_vw_1 <- subset(stb_b_sum_2, VW.x == -999 | VW.y == -999)

# Identify similar records
fil_vw_2 <- subset(stb_b_sum_2, record_VW == "same")


# 18) Column VW_MAX (maximum wind velocity, unit = m/s)

# Plot for difference distribution
ggplot(data = stb_b_sum_2) +
  geom_point(aes(x = timestamp, y = difference_VW_MAX), size = 0.01, colour = c("darkgrey"))

# Identify columns with -999
fil_vw_max_1 <- subset(stb_b_sum_2, VW_MAX.x == -999 | VW_MAX.y == -999)

# Identify similar records
fil_vw_max_2 <- subset(stb_b_sum_2, record_VW_MAX == "same")

