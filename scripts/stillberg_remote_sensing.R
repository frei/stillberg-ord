# INFO ----

# This script is used to quantify the differences between the Lidar survey and 
# the field measurements collected during the 2015/2016 Stillberg remote sensing project
# For parameters basal heigt, lot height and biomass
# For UAV and LAS surveys separately
# Obtained for plots (EFs) via multiple regression analysis
# Author: Lia Lechler
# Starting Date: 21/02/2022


# SETTING UP WORKSPACE ----

# Clean workspace
rm(list = ls())

# Load packages useful for data manipulation
library(tidyverse)
library(dplyr)
library(ggplot2)
library(ggpubr)
library(gridExtra)
library(knitr)
library(yardstick)
library(lme4)

# Set working directory
setwd("C:/Git/stillberg-ord")

# Read csv files
rem <- read.csv("data/remsens/02_03_23_modelled_parameters.csv")
field <- read.csv("data/remsens/27_03_23_field_parameters.csv")


# JOIN TABLES FOR ANALYSIS ----

# join datasets by plot
ana <- left_join(rem, field, by = "EF")


# FILTER VARIABLES ----

# filter all values from ALS analysis for mean is <16 and UAV analysis >1700
ana_filt <- ana %>% 
  filter(LAS_lot15_mean_m >= 16) %>% 
  filter(UAV_lot15_mean_m <= 1700)

ana_filt <- ana


# EXPLORE DATA ----

# Count the number of plots with each number of trees
# NOTE: Equal counts for RS and F, so just one plot
tree_counts <- ana_filt %>%
  group_by(n_Trees.x) %>%
  summarize(count = n())

# Create bar plot of the counts
(plot_counts <- ggplot(tree_counts, aes(x = factor(n_Trees.x), y = count, fill = factor(n_Trees.x))) +
  geom_bar(stat = "identity", color = "black", size = 0.25, width = 0.75, fill = "dodgerblue4", alpha = 0.7) +
  labs(x = "Number of trees", y = "Number of plots") +
  theme_classic() +
  theme(axis.text = element_text(size = 12, color = "black"),
        axis.title = element_text(size = 14, color = "black"),
        legend.title = element_blank(),
        legend.text = element_text(size = 12, color = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()) +
  ggtitle("") +
  theme(plot.title = element_text(size = 16, face = "bold", hjust = 0.5, color = "black")))

# save the plot as a PNG file with a specified width and height
ggsave("figures/remote_sensing/plot_counts.png", plot = plot_counts, width = 6, height = 4, dpi = 300)


# Count the number of plots with each tree type
# NOTE: Equal counts for RS and F, so just one plot
tree_types <- ana_filt %>%
  group_by(Tree_Type.x) %>%
  summarize(count = n())

# Create bar plot of the counts with custom x-axis labels
(species_counts <- ggplot(tree_types, aes(x = factor(Tree_Type.x), y = count, fill = factor(Tree_Type.x))) +
    geom_bar(stat = "identity", color = "black", size = 0.25, width = 0.5, alpha = 0.5) +
    scale_fill_manual(values = c("darkolivegreen", "darkgray", "darkslategrey")) +
    labs(x = "Tree species", y = "Number of plots") +
    scale_x_discrete(labels = c("P. cembra", "P. mugo", "L. decidua")) +
    theme_classic() +
    theme(axis.text = element_text(size = 12, color = "black"),
          axis.title = element_text(size = 14, color = "black"),
          legend.title = element_blank(),
          legend.position = "none",
          legend.text = element_text(size = 12, color = "black"),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank()) +
    ggtitle("") +
    theme(plot.title = element_text(size = 16, face = "bold", hjust = 0.5, color = "black")))

# save the plot as a PNG file with a specified width and height
ggsave("figures/remote_sensing/species_counts.png", plot = species_counts, width = 6, height = 4, dpi = 300)


# ANALYSE PARAMETER DIFFERENCES PART 1 ----

# Calculate relative  differences (if one plot picked, how big would the difference be)
diff_rel <- ana_filt %>% 
  mutate(#lot_max_uav = abs(UAV_lot15_max_m - Lot15_max),
         #lot_max_las = abs(LAS_lot15_max_m - Lot15_max),
         lot_mean_uav = abs(UAV_lot15_mean_m - Lot15_mean),
         lot_mean_las = abs(LAS_lot15_mean_m - Lot15_mean))

# Check column types
str(diff_rel)

# Calculate mean per parameter
diff_rel_2 <- diff_rel %>% 
  summarize(#mean_lot_max_uav = mean(lot_max_uav),
         #mean_lot_max_las = mean(lot_max_las),
         mean_lot_mean_uav = mean(lot_mean_uav),
         mean_lot_mean_las = mean(lot_mean_las))

# Create table
(tab_diff_rel <- kable(diff_rel_2))


# ANALYSE PARAMETER DIFFERENCES PART 2 ----

# Calculate absolute differences (over- and underestimation across all plots)

diff_abs <- ana_filt %>% 
  mutate(#diff_lot_max_uav = UAV_lot15_max_m - Lot15_max,
         #height_diff_direction_max_uav = ifelse(diff_lot_max_uav > 0, "Overestimate", "Underestimate"),
         
         #diff_lot_max_las = LAS_lot15_max_m - Lot15_max,
         #height_diff_direction_max_las = ifelse(diff_lot_max_las > 0, "Overestimate", "Underestimate"),
         
         diff_lot_mean_uav = UAV_lot15_mean_m - Lot15_mean,
         height_diff_direction_mean_uav = ifelse(diff_lot_mean_uav > 0, "Overestimate", "Underestimate"),
         
         diff_lot_mean_las = LAS_lot15_mean_m - Lot15_mean,
         height_diff_direction_mean_las = ifelse(diff_lot_mean_las > 0, "Overestimate", "Underestimate"))

diff_abs_2 <- diff_abs %>% 
  summarize(#mean_height_diff_lot_max_uav = mean(diff_lot_max_uav),
            #mean_height_diff_lot_max_las = mean(diff_lot_max_las),
            mean_height_diff_lot_mean_uav = mean(diff_lot_mean_uav),
            mean_height_diff_lot_mean_las = mean(diff_lot_mean_las))

# Create table
(tab_diff_abs <- kable(diff_abs_2))


# VISUALIZE RESULTS ----

# UAV colours
uav <- c("darkgoldenrod4", "darkseagreen4")
las <- c("dimgray", "firebrick4")

# Plot all parameters individually

# (plot_uav_max <- ggplot(diff_abs, aes(x = Lot15_max, y = UAV_lot15_max_m, color = height_diff_direction_max_uav)) +
#   geom_point(size = 2, alpha = 0.5) +
#   #geom_smooth(method = "lm", se = TRUE) +
#   labs(x = "Field height max (cm)", y = "Remote sensing height max (cm)", color = "Height difference") +
#   xlim(0, max(diff_abs$Lot15_max, diff_abs$UAV_lot15_max_m) * 1.1) +
#   ylim(0, max(diff_abs$Lot15_max, diff_abs$UAV_lot15_max_m) * 1.1) +
#   scale_color_manual(values = uav) +
#   ggtitle("UAV") +
#   theme_classic() +
#   theme(axis.title = element_text(size = 12), axis.text = element_text(size = 10),
#         legend.title = element_text(size = 10), legend.position = "off", legend.text = element_text(size = 8)))

# (plot_las_max <- ggplot(diff_abs, aes(x = Lot15_max, y = LAS_lot15_max_m, color = height_diff_direction_max_las)) +
#     geom_point(size = 2, alpha = 0.5) +
#     labs(x = "Field height max (cm)", y = "Remote sensing height max (cm)", color = "Height difference") +
#     xlim(0, max(diff_abs$Lot15_max, diff_abs$LAS_lot15_max_m) * 1.1) +
#     ylim(0, max(diff_abs$Lot15_max, diff_abs$LAS_lot15_max_m) * 1.1) +
#     scale_color_manual(values = las) +
#     ggtitle("ALS") +
#     theme_classic() +
#     theme(axis.title = element_text(size = 12), axis.text = element_text(size = 10),
#           legend.title = element_text(size = 10), legend.position = "off", legend.text = element_text(size = 8)))

(plot_uav_mean <- ggplot(diff_abs, aes(x = Lot15_max, y = UAV_lot15_mean_m, color = height_diff_direction_mean_uav)) +
    geom_point(size = 2, alpha = 0.7) +
    labs(x = "Field height mean (cm)", y = "Remote sensing height mean (cm)", color = "Height difference") +
    xlim(0, max(diff_abs$Lot15_mean, diff_abs$UAV_lot15_mean_m) * 1.1) +
    ylim(0, max(diff_abs$Lot15_mean, diff_abs$UAV_lot15_mean_m) * 1.1) +
    scale_color_manual(values = uav) +
    ggtitle("UAV") +
    theme_classic() +
    theme(axis.title = element_text(size = 12), axis.text = element_text(size = 10),
          legend.title = element_text(size = 10), legend.position = "bottom", legend.text = element_text(size = 8)))

(plot_las_mean <- ggplot(diff_abs, aes(x = Lot15_mean, y = LAS_lot15_mean_m, color = height_diff_direction_mean_las)) +
    geom_point(size = 2, alpha = 0.7) +
    labs(x = "Field height mean (cm)", y = "Remote sensing height mean (cm)", color = "Height difference") +
    xlim(0, max(diff_abs$Lot15_mean, diff_abs$LAS_lot15_mean_m) * 1.1) +
    ylim(0, max(diff_abs$Lot15_mean, diff_abs$LAS_lot15_mean_m) * 1.1) +
    scale_color_manual(values = las) +
    ggtitle("ALS") +
    theme_classic() +
    theme(axis.title = element_text(size = 12), axis.text = element_text(size = 10),
          legend.title = element_text(size = 10), legend.position = "bottom", legend.text = element_text(size = 8)))

# Create panel
estimation <- grid.arrange(#plot_uav_max, #plot_las_max, 
  plot_uav_mean, plot_las_mean, ncol = 2)

# save the plot as a PNG file with a specified width and height
ggsave("figures/remote_sensing/methods_estimation_mean.png", plot = estimation, width = 8, height = 4, dpi = 300)


# # RMSE
# rmse_uav_mean <- rmse(data = ana_filt, UAV_lot15_mean_m, Lot15_mean)
# 
# # LINEAR MODELS ----
# lm_uav_mean <- lm(UAV_lot15_mean_m ~ Lot15_mean, data = ana_filt)
# lm_las_mean <- lm(LAS_lot15_mean_m ~ Lot15_mean + Tree_Type.x, data = ana_filt)
# 
# summary(lm_uav_mean)
# summary(lm_las_mean)
# 
# plot(lm_uav_mean)
# predict(lm_uav_mean)
# 
# 
# # LINEAR MIXED EFFECT MODELS ----
# lme_uav_mean <- lmer(UAV_lot15_mean_m ~ Lot15_mean + (1|Tree_Type.x), data = ana_filt)
# 
# summary(lme_uav_mean)


# ANALYSE SPECIES DIFFERENCES ----

# Boxplots
ana_filt_2 <- ana_filt %>% 
  mutate(Tree_Type.x = case_when(
    Tree_Type.x == 1 ~ "P. cembra",
    Tree_Type.x == 2 ~ "P. mugo",
    Tree_Type.x == 3 ~ "L. decidua",
    TRUE ~ as.character(Tree_Type.x) # If there are other values not accounted for, keep them as is
  ))

# # create boxplot using ggplot2
# (df <- ana_filt %>%
#   gather(key = "measurement", value = "height", UAV_lot15_mean_m, Lot15_mean) %>%
#   ggplot(aes(x = Tree_Type.x, y = height, fill = measurement)) +
#   geom_boxplot() +
#   facet_wrap(~ Tree_Type.x, ncol = 3) +
#   labs(x = "Tree Species", y = "Height (m)", fill = "Measurement"))
# 
# 
# # create boxplot using ggplot2
# (df <- diff_abs %>%
#     ggplot(aes(x = Tree_Type.x, y = diff_lot_mean_uav, group = Tree_Type.x)) +
#     geom_boxplot() +
#     #facet_wrap(~ Tree_Type.x, ncol = 3) +
#     labs(x = "Tree Species", y = "Height (m)", fill = "Measurement"))

(uav_max <- ana_filt_2 %>%
    gather(key = "measurement", value = "height", UAV_lot15_max_m, Lot15_max) %>%
    ggplot(aes(x = Tree_Type.x, y = height, fill = measurement)) +
    geom_boxplot(width = 0.3, notch = TRUE, outlier.shape = NA) +
    coord_cartesian(ylim = c(0, 900)) +
    labs(x = "Tree Species", y = "Max Height (cm)", fill = "") +
    scale_fill_manual(values = c("royalblue", "sienna2"), labels = c("Remote sensing", "Field measurement")) +
    ggtitle("UAV") +
    theme_classic() +
    theme(axis.title = element_text(size = 14), axis.text = element_text(size = 12),
          legend.title = element_text(size = 12), legend.position = "", legend.text = element_text(size = 10)))

(als_max <- ana_filt_2 %>%
    gather(key = "measurement", value = "height", LAS_lot15_max_m, Lot15_max) %>%
    ggplot(aes(x = Tree_Type.x, y = height, fill = measurement)) +
    geom_boxplot(width = 0.3, notch = TRUE, outlier.shape = NA) +
    coord_cartesian(ylim = c(0, 900)) +
    labs(x = "Tree Species", y = "Max Height (cm)", fill = "") +
    scale_fill_manual(values = c("royalblue4", "sienna"), labels = c("Remote sensing", "Field measurement")) +
    ggtitle("ALS") +
    theme_classic() +
    theme(axis.title = element_text(size = 14), axis.text = element_text(size = 12),
          legend.title = element_text(size = 12), legend.position = "", legend.text = element_text(size = 10)))

(uav_mean <- ana_filt_2 %>%
  gather(key = "measurement", value = "height", UAV_lot15_mean_m, Lot15_mean) %>%
  ggplot(aes(x = Tree_Type.x, y = height, fill = measurement)) +
  geom_boxplot(width = 0.3, notch = TRUE, outlier.shape = NA) +
    coord_cartesian(ylim = c(0, 900)) +
  labs(x = "Tree Species", y = "Mean Height (cm)", fill = "") +
  scale_fill_manual(values = c("royalblue", "sienna2"), labels = c("Remote sensing", "Field measurement")) +
  ggtitle("UAV") +
  theme_classic() +
  theme(axis.title = element_text(size = 14), axis.text = element_text(size = 12),
          legend.title = element_text(size = 12), legend.position = "bottom", legend.text = element_text(size = 10)))

(als_mean <- ana_filt_2 %>%
    gather(key = "measurement", value = "height", LAS_lot15_mean_m, Lot15_mean) %>%
    ggplot(aes(x = Tree_Type.x, y = height, fill = measurement)) +
    geom_boxplot(width = 0.3, notch = TRUE, outlier.shape = NA) +
    coord_cartesian(ylim = c(0, 900)) +
    labs(x = "Tree Species", y = "Mean Height (cm)", fill = "") +
    scale_fill_manual(values = c("royalblue4", "sienna"), labels = c("Remote sensing", "Field measurement")) +
    ggtitle("ALS") +
    theme_classic() +
    theme(axis.title = element_text(size = 14), axis.text = element_text(size = 12),
          legend.title = element_text(size = 12), legend.position = "bottom", legend.text = element_text(size = 10)))

# Create panel
species <- grid.arrange(uav_max, als_max, uav_mean, als_mean, ncol = 2)

# save the plot as a PNG file with a specified width and height
ggsave("figures/remote_sensing/species_estimation_2.png", plot = species, width = 10, height = 8, dpi = 300)



