# INFO ----

# This script is used to create a timeline for all studies conducted as part 
# of the Stillberg afforestation site
# Author: Lia Lechler
# Starting Date: 21/02/2022


# SETTING UP WORKSPACE ----

# Clean workspace
rm(list = ls())

# Load packages useful for data manipulation
library(dplyr)
library(lubridate)
library(ggplot2)
library(forcats)
library(stringr)

# Define colours
colours <- c("darkred", #Afforestation
             "darkgrey", #Met Records
             "deepskyblue4", #FACE
             "darkorange3", #NutAdd
             "aquamarine4") #GTREE

# Define dataframe
experiments <- data.frame(
  experiment = factor(c("Afforestation", "Meteorological Station", "FACE", "Nutrient Addition", "GTREE"), 
                      levels = c("GTREE", "Nutrient Addition", "FACE", "Meteorological Station", "Afforestation")),
  start_date = ymd(c("1975-01-01", "1975-01-01", "2001-01-01", "2004-01-01", "2018-01-01")),
  end_date = ymd(c("2015-12-31", "2022-12-31", "2012-12-31", "2016-12-31", "2022-12-31"))
)

# Create plot
ggplot(experiments, aes(x = start_date, xend = end_date, y = experiment, yend = experiment)) +
  geom_segment(size = 10, alpha = 0.7, color = colours) +
  labs(x = "Year", y = "") +
  scale_x_date(date_labels = "%Y", date_breaks = "5 years") +
  theme_minimal() +
  theme(axis.text.x = element_text(size = 12),
        axis.text.y = element_text(size = 12),
        axis.title = element_text(size = 16),
        plot.title = element_text(size = 18))


# LABELS ----

# Define colours
colours <- c("darkred", #Afforestation
             "darkgrey", #Met Records
             "deepskyblue4", #FACE
             "darkorange3", #NutAdd
             "aquamarine4") #GTREE

# Define dataframe
experiments <- data.frame(
  experiment = factor(c("Afforestation", "Meteorological Station", "FACE", "Nutrient Addition", "GTREE"), 
                      levels = c("GTREE", "Nutrient Addition", "FACE", "Meteorological Station", "Afforestation")),
  start_date = ymd(c("1975-01-01", "1975-01-01", "2001-01-01", "2004-01-01", "2013-01-01")),
  end_date = ymd(c("2015-12-31", "2022-12-31", "2012-12-31", "2016-12-31", "2022-12-31")),
  label = c("ongoing", "ongoing", "ongoing", "ongoing", "ongoing")
)

# Create plot
ggplot(experiments, aes(x = start_date, xend = end_date, y = experiment, yend = experiment)) +
  geom_segment(size = 10, alpha = 0.7, color = colours) +
  geom_text(aes(x = end_date, y = experiment, label = label), size = 4, hjust = -0.1) +
  labs(x = "Year", y = "") +
  scale_x_date(date_labels = "%Y", date_breaks = "5 years") +
  theme_minimal() +
  theme(axis.text.x = element_text(size = 12),
        axis.text.y = element_blank(),
        axis.title = element_text(size = 16),
        plot.title = element_text(size = 18))


# SECOND BARS ----

# Define colours
colours <- c("deepskyblue4", #Afforestation
             "darkgrey", #Met Records
             "deepskyblue4", #FACE
             "darkorange3", #NutAdd
             "aquamarine4") #GTREE

# Define experiments and add a new column for ongoing experiments
experiments <- data.frame(
  experiment = factor(c("Afforestation", "Meteorological Station", "FACE", "Nutrient Addition", "G-TREE", 
                        "Afforestation", "Meteorological Station", "Nutrient Addition", "G-TREE"), 
                      levels = c("G-TREE", "Nutrient Addition", "FACE", "Meteorological Station", "Afforestation")),
  start_date = ymd(c("1954-01-01", "1975-01-01", "2001-01-01", "2004-01-01", "2013-01-01", 
                     "2015-12-31", "2022-12-31", "2016-12-31", "2022-12-31")),
  end_date = ymd(c("2015-12-31", "2022-12-31", "2012-12-31", "2016-12-31", "2022-12-31",
                   "2025-12-31", "2025-12-31", "2025-12-31", "2025-12-31")),
  Ongoing = c(rep("Data Presented", 5), rep("Ongoing Data Collection", 4))
)

(final <- ggplot(experiments, aes(x = start_date, xend = end_date, y = experiment, yend = experiment, colour = Ongoing)) +
  geom_segment(size = 10, alpha = 0.7) +
  scale_color_manual(values = c(colours, alpha(colours, 0.3))) +
  labs(x = "", y = "") +
  scale_x_date(date_labels = "%Y", date_breaks = "5 years") +
  theme_minimal() +
  theme(axis.text.x = element_text(size = 14),
        axis.text.y = element_text(size = 16),
        axis.title = element_text(size = 18),
        legend.title = element_blank(),
        legend.text = element_text(size = 16),
        legend.position = "bottom"))


final +                                # Modify labels of ggplot2 barplot
  scale_y_discrete(labels = function(y) str_wrap(y, width = 10))

